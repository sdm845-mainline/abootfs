#pragma once

#include <string>

namespace afs
{

// cmdline options
struct opts {
	// The source device/file to mount
	std::string mnt_source;
	// Target mountpoint
	std::string mnt_target;
};

} // namespace ab
