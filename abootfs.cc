#include <iostream>
#include <vector>
#include <memory>
#include <unistd.h>

#include <fuse.h>

#include "abootfs.hh"

void usage() {
	std::cout << "abootFS: Android boot image FUSE mounter" << std::endl;
	std::cout << "abootFS: [-h] [-c] < source to mount > < target directory >" << std::endl;
	std::cout << "-h			This help text" << std::endl;
	std::cout << "-c			Config file to use instead of guessing layout (No implemented)" << std::endl;
	exit(1);
}

struct afs::opts parseOpts(int argc, char* argv[]) {
	struct afs::opts opts;
	if (argc == 1)
		usage();
	auto optStrings = std::vector<std::string>(argv + 1, argv + argc);

	std::string o = optStrings.front();
	for (; optStrings.size() > 0; o = optStrings.front())
	{
		if (o == "-h")
			usage();
		else if (o.at(0) != '-') { // Kinda bad, but nobody starts a filename with a - right?
			if (opts.mnt_source.size() == 0)
				opts.mnt_source = o;
			else
				opts.mnt_target = o;
		}

		optStrings.erase(optStrings.begin());
	}
}

int main(int argc, char* argv[])
{
	auto opts = parseOpts(argc, argv);
	printf("Fuse library version %d.%d\n", FUSE_MAJOR_VERSION, FUSE_MINOR_VERSION);
	return 0;
}
